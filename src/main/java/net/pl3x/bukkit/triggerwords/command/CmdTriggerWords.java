package net.pl3x.bukkit.triggerwords.command;

import net.pl3x.bukkit.triggerwords.Logger;
import net.pl3x.bukkit.triggerwords.TriggerWords;
import net.pl3x.bukkit.triggerwords.configuration.Config;
import net.pl3x.bukkit.triggerwords.configuration.Lang;
import net.pl3x.bukkit.triggerwords.configuration.Triggers;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.Collections;
import java.util.List;

public class CmdTriggerWords implements TabExecutor {
    private TriggerWords plugin;

    public CmdTriggerWords(TriggerWords plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1 && "reload".startsWith(args[0].toLowerCase())) {
            return Collections.singletonList("reload");
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.triggerwords")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
            Logger.debug("Reloading config...");
            Config.reload();

            Logger.debug("Reloading language file...");
            Lang.reload();

            Logger.debug("Reloading triggers file...");
            Triggers.reloadConfig();

            Lang.send(sender, Lang.RELOAD
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()));
            return true;
        }

        Lang.send(sender, Lang.VERSION
                .replace("{version}", plugin.getDescription().getVersion())
                .replace("{plugin}", plugin.getName()));
        return true;
    }
}
