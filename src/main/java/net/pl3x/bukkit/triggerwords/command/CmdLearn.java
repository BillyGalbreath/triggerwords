package net.pl3x.bukkit.triggerwords.command;

import net.pl3x.bukkit.triggerwords.configuration.Lang;
import net.pl3x.bukkit.triggerwords.configuration.Triggers;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CmdLearn implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Triggers.getConfig().getKeys(false).stream()
                    .filter(trigger -> trigger.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toCollection(ArrayList::new));
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.learn")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            Lang.send(sender, Lang.INVALID_TRIGGER);
            return true;
        }

        if (args.length == 1) {
            Lang.send(sender, Lang.INVALID_RESPONSE);
            return true;
        }

        String trigger = args[0];
        String response = String.join(" ", Arrays.asList(args)).replace(trigger + " ", "");

        Triggers.learn(trigger, response);
        Lang.send(sender, Lang.RESPONSE_LEARNED
                .replace("{trigger}", trigger)
                .replace("{response}", response));
        return true;
    }
}
