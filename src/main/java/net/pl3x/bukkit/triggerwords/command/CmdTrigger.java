package net.pl3x.bukkit.triggerwords.command;

import net.pl3x.bukkit.triggerwords.configuration.Lang;
import net.pl3x.bukkit.triggerwords.configuration.Triggers;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CmdTrigger implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Triggers.getConfig().getKeys(false).stream()
                    .filter(trigger -> trigger.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toCollection(ArrayList::new));
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.trigger")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            Set<String> triggers = Triggers.getConfig().getKeys(false);
            if (triggers == null || triggers.isEmpty()) {
                Lang.send(sender, Lang.NO_LEARNED_TRIGGERS);
                return true;
            }
            sender.sendMessage(triggers.toString().replace("[", "").replace("]", ""));
            return true;
        }

        String trigger = args[0];
        if (trigger == null || trigger.isEmpty()) {
            Lang.send(sender, Lang.INVALID_TRIGGER);
            return true; // not a trigger
        }

        String response = Triggers.getResponse(trigger);
        if (response == null) {
            Lang.send(sender, Lang.INVALID_TRIGGER);
            return true; // not a trigger
        }

        Lang.send(sender, Lang.TRIGGER_RESPONSE
                .replace("{trigger}", trigger)
                .replace("{response}", response));
        return true;
    }
}
