package net.pl3x.bukkit.triggerwords.configuration;

import net.pl3x.bukkit.triggerwords.TriggerWords;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Triggers extends YamlConfiguration {
    private static Triggers config;

    public static Triggers getConfig() {
        if (config == null) {
            config = new Triggers();
        }
        return config;
    }

    public static String getResponse(String trigger) {
        return getConfig().getString(trigger);
    }

    public static void learn(String trigger, String response) {
        getConfig().set(trigger, response);
        getConfig().save();
    }

    public static void forget(String trigger) {
        getConfig().set(trigger, null);
        getConfig().save();
    }

    public static void reloadConfig() {
        getConfig().reload();
    }

    private final File file;
    private final Object saveLock = new Object();

    private Triggers() {
        super();
        this.file = new File(TriggerWords.getPlugin().getDataFolder(), "triggers.yml");
        reload();
    }

    private void reload() {
        synchronized (saveLock) {
            try {
                this.load(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void save() {
        synchronized (saveLock) {
            try {
                this.save(file);
            } catch (Exception ignore) {
            }
        }
    }
}
