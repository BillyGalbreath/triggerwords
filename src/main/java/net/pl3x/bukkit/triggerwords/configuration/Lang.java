package net.pl3x.bukkit.triggerwords.configuration;

import net.pl3x.bukkit.triggerwords.TriggerWords;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION = "&4You do not have permission for that command!";
    public static String NO_LEARNED_TRIGGERS = "&4No learned triggers!";
    public static String INVALID_TRIGGER = "&4Invalid trigger specified!";
    public static String INVALID_RESPONSE = "&4Invalid response specified!";
    public static String RESPONSE_LEARNED = "&dLearned &7{trigger}&e: &r{response}";
    public static String RESPONSE_FORGOTTEN = "&dForgot &7{trigger}";
    public static String TRIGGER_RESPONSE = "&d{trigger}&e: &r{response}";
    public static String VERSION = "&d{plugin} v{version}.";
    public static String RELOAD = "&d{plugin} v{version} reloaded.";

    public static void reload() {
        TriggerWords plugin = TriggerWords.getPlugin();
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for that command!");
        NO_LEARNED_TRIGGERS = config.getString("no-learned-triggers", "&4No learned triggers!");
        INVALID_TRIGGER = config.getString("invalid-trigger", "&4Invalid trigger specified!");
        INVALID_RESPONSE = config.getString("invalid-response", "&4Invalid response specified!");
        RESPONSE_LEARNED = config.getString("response-learned", "&dLearned &7{trigger}&e: &r{response}");
        RESPONSE_FORGOTTEN = config.getString("response-forgotten", "&dForgot &7{trigger}");
        TRIGGER_RESPONSE = config.getString("trigger-response", "&d{trigger}&e: &r{response}");
        VERSION = config.getString("version", "&d{plugin} v{version}.");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
