package net.pl3x.bukkit.triggerwords.configuration;

import net.pl3x.bukkit.triggerwords.TriggerWords;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static Boolean SHORT_PLAYER_TRIGGERS = true;
    public static Boolean SHORT_SERVER_TRIGGERS = false;

    public static void reload() {
        TriggerWords plugin = TriggerWords.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        SHORT_PLAYER_TRIGGERS = config.getBoolean("short-player-triggers", true);
        SHORT_SERVER_TRIGGERS = config.getBoolean("short-server-triggers", false);
    }
}
