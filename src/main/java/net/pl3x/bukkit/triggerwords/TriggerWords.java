package net.pl3x.bukkit.triggerwords;

import net.pl3x.bukkit.triggerwords.command.CmdForget;
import net.pl3x.bukkit.triggerwords.command.CmdLearn;
import net.pl3x.bukkit.triggerwords.command.CmdTrigger;
import net.pl3x.bukkit.triggerwords.command.CmdTriggerWords;
import net.pl3x.bukkit.triggerwords.configuration.Config;
import net.pl3x.bukkit.triggerwords.configuration.Lang;
import net.pl3x.bukkit.triggerwords.listener.TriggerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class TriggerWords extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        Bukkit.getPluginManager().registerEvents(new TriggerListener(), this);

        getCommand("triggerwords").setExecutor(new CmdTriggerWords(this));
        getCommand("trigger").setExecutor(new CmdTrigger());
        getCommand("learn").setExecutor(new CmdLearn());
        getCommand("forget").setExecutor(new CmdForget());

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static TriggerWords getPlugin() {
        return TriggerWords.getPlugin(TriggerWords.class);
    }
}
