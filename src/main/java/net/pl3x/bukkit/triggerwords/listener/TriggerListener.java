package net.pl3x.bukkit.triggerwords.listener;

import net.pl3x.bukkit.triggerwords.configuration.Config;
import net.pl3x.bukkit.triggerwords.configuration.Lang;
import net.pl3x.bukkit.triggerwords.configuration.Triggers;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

public class TriggerListener implements Listener {
    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommand(PlayerCommandPreprocessEvent event) {
        if (!Config.SHORT_PLAYER_TRIGGERS) {
            return;
        }

        String trigger = event.getMessage();
        trigger = trigger.substring(1, trigger.length());
        if (trigger.equals("") || trigger.isEmpty()) {
            return; // not a trigger
        }

        String response = Triggers.getResponse(trigger);
        if (response == null) {
            return; // not a trigger
        }

        Lang.send(event.getPlayer(), Lang.TRIGGER_RESPONSE
                .replace("{trigger}", trigger)
                .replace("{response}", response));
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommand(ServerCommandEvent event) {
        if (!Config.SHORT_SERVER_TRIGGERS) {
            return;
        }

        String trigger = event.getCommand();
        if (trigger == null || trigger.isEmpty()) {
            return; // not a trigger
        }

        String response = Triggers.getResponse(trigger);
        if (response == null) {
            return; // not a trigger
        }

        Lang.send(event.getSender(), Lang.TRIGGER_RESPONSE
                .replace("{trigger}", trigger)
                .replace("{response}", response));
        event.setCancelled(true);
    }
}
